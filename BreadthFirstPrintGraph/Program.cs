﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreadthFirstPrintGraph
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Member
    {
        public String name;
        public String email;
        public List<Member> friends;
    }

    class SocialGraph
    {
        void printSocialGraph(Member member)
        {
            if (member == null)
                throw new ArgumentOutOfRangeException("member");

            Queue<Member> QueueOfMemebers = new Queue<Member>();
            HashSet<Member> Printed = new HashSet<Member>();
            QueueOfMemebers.Enqueue(member);
            Printed.Add(member);

            int level = 1;

            Console.WriteLine("Level " + level + " friends");

            while (!(QueueOfMemebers.Count == 0))
            {
                Member tempMember = QueueOfMemebers.Dequeue();
                List<Member> TempMemberFriendList = tempMember.friends;

                foreach (var friend in TempMemberFriendList)
                {
                    if (Printed.Contains(friend)) continue;

                    Printed.Add(friend);
                    Console.WriteLine("Name:" + friend.name + ", Email:" + friend.email);
                    foreach (var friendOfFriend in friend.friends)
                        QueueOfMemebers.Enqueue(friendOfFriend);
                }

                level++;
                Console.WriteLine("Level " + level + " friends");
            }
        }

    }
}
